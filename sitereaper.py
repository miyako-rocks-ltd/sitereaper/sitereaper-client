import requests
import json

HOST = "http://127.0.0.1:3000"

def filterDict(listObj, filterBy, query):
    newList = list()
    for obj in listObj:
        if obj[filterBy] == query:
            newList.append(obj)   
    return newList

def searchTitle(query, exact=False):
    if exact:
        response = requests.get(f'{HOST}/search?query', params={'query': query}).json()
        return filterDict(response, 'title', query)
    else:
        return requests.get(f'{HOST}/search?query', params={'query': query}).json()
